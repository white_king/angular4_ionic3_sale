import {ErrorHandler, NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";
import {IonicApp, IonicErrorHandler, IonicModule} from "ionic-angular";
import {MyApp} from "./app.component";

import {HomePage} from "../pages/home/home";
import {TabsPage} from "../pages/tabs/tabs";

import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";
import {CartPage} from "../pages/cart/cart";
import {MePage} from "../pages/me/me";
import {CommonService} from "../pages/service/CommonService";
import {LocationPage} from "../pages/location/location";
import {BindPhonePage} from "../pages/bind-phone/bind-phone";
import {DetailPage} from "../pages/detail/detail";
import {ConfirmOrderPage} from "../pages/confirm-order/confirm-order";
import {FeedbackPage} from "../pages/feedback/feedback";
import {EventBusService} from "../pages/service/EventBusService";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CartPage,
    MePage,
    TabsPage,
    LocationPage,
    BindPhonePage,
    DetailPage,
    ConfirmOrderPage,
    FeedbackPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: 'true',
    }),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CartPage,
    MePage,
    HomePage,
    TabsPage,
    LocationPage,
    BindPhonePage,
    DetailPage,
    ConfirmOrderPage,
    FeedbackPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CommonService,
    EventBusService
  ]
})
export class AppModule {
}
