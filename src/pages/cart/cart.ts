import {Component, OnInit} from "@angular/core";
import {EventBusService} from "../service/EventBusService";

@Component({
  selector: 'page-cart',
  templateUrl: './cart.html'
})
export class CartPage implements OnInit {
  shoppingCartList: any = [];

  constructor(public eventBusService: EventBusService) {

  }

  ngOnInit(): void {
    console.log('cart init');
    this.shoppingCartList.length = 0;

    this.eventBusService.eventBus.subscribe(value => {
      this.shoppingCartList = value;

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }
}
