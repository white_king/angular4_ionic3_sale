import {Component, OnInit} from "@angular/core";
import {NavController} from "ionic-angular";
import {BindPhonePage} from "../bind-phone/bind-phone";


@Component({
  selector: 'page-me',
  templateUrl: './me.html'
})
export class MePage implements OnInit {
  ngOnInit(): void {
    console.log('me init');
  }

  defaultName: string = '狂欢型杀人犯';
  defaultPoint: number = 1000;
  defaultImg: string = '../../assets/images/jjs.jpg';

  constructor(public navCtrl: NavController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MePage');
  }

  viewWillAppear() {
    console.log('viewWillAppear');
  }


  bindPhone() {
    this.navCtrl.push(BindPhonePage, {
      callback: this.getData
    });
  }

  getData = (data) => {
    return new Promise((resolve, reject) => {
      const info = JSON.parse(data);
      this.defaultName = info.nickname;
      this.defaultPoint = info.point;
      this.defaultImg = info.image;
    })
  };

  userName: string = 'userName';

}
