import {Component} from "@angular/core";
import {IonicPage, NavController, NavParams} from "ionic-angular";
import {Utils} from "../utils";
import {CommonService} from "../service/CommonService";

/**
 * Generated class for the BindPhonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bind-phone',
  templateUrl: 'bind-phone.html',
})
export class BindPhonePage {

  phoneNum: string = '15216408861';
  validTime: number;
  code: string = '';
  codeText: string = '获取验证码';
  countDown: number = 60;
  touchCode: boolean = false;

  nickname: string;
  image: string;
  point: number;

  callback: any;
  data: string;


  constructor(public navCtrl: NavController, public navParams: NavParams, private service: CommonService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BindPhonePage');
    this.callback = this.navParams.get('callback');
  }

  // 关闭按钮，返回"我的"页面
  close(): void {
    this.callback(window.localStorage.getItem('userInfo')).then(() => {
      this.navCtrl.pop()
    });
    this.navCtrl.pop();
  }


  // 获取手机验证码
  getAuthCode(num: string) {
    if (this.touchCode) {
      return;
    }
    if (!Utils.testPhone(num)) {
      alert('手机号码格式有误！');
      this.phoneNum = '';
      return;
    } else {
      this.service.postData('customer/sendCode', this.phoneNum, 1).subscribe(res => {

        if (res.validTime === 0) {
          alert('验证码请求频繁，请稍后重试。');
          return;
        }
        // 验证码
        this.code = res.code;
        // 有效时间
        this.validTime = res.validTime;

        this.codeText = `${this.countDown}s后重发`;
        this.touchCode = true;

        let timer = setInterval(() => {
          --this.countDown;
          this.codeText = `${this.countDown}s后重发`;
          if (this.countDown === 0) {
            this.codeText = '获取验证码';
            this.touchCode = false;
            this.countDown = 60;
            clearInterval(timer);
          }
        }, 1000);
      });
    }
  }

  // 绑定手机
  bindPhone() {
    if (this.phoneNum === '') {
      alert('手机号不能为空！');
      return;
    } else if (this.code === '') {
      alert('验证码不能为空！');
      return;
    } else if (!Utils.testPhone(this.phoneNum)) {
      alert('手机号码格式有误！');
      this.phoneNum = '';
      return;
    }

    this.service.postData('customer/bindPhone', this.phoneNum, parseInt(this.code)).subscribe(res => {
        this.nickname = res.nickname;
        this.image = res.image;
        this.point = res.point;

        let userInfo = {
          nickname: this.nickname,
          image: this.image,
          point: this.point
        };

        localStorage.setItem('userInfo', JSON.stringify(userInfo));

        alert('绑定成功！2秒后跳回"我的"');
        setTimeout(() => {
          this.callback(window.localStorage.getItem('userInfo')).then(() => {
            this.navCtrl.pop();
          });
          this.navCtrl.pop();
        }, 2000);
      }
    );
  }
}















