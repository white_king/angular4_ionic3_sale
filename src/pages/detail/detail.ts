import {Component, OnInit} from "@angular/core";
import {IonicPage, NavController, NavParams} from "ionic-angular";
import {ConfirmOrderPage} from "../confirm-order/confirm-order";
import {Combo, Product} from "../model";
import Swiper from "swiper";

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage implements OnInit {

  goods: any;
  product: Product;
  combo: Combo;
  maxInventory: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }

  ngOnInit(): void {
    this.goods = this.navParams.data.goods;
    this.goods.productId != null ? this.product = this.goods : this.combo = this.goods;

    if (this.combo) {
      this.maxInventory = this.combo.comboId != null ? 2 : 1;
    } else if (this.product) {
      // this.maxInventory = this.product.productId != null ? this.product.leftInventory : 1;
    }


    new Swiper('.swiper-container', {
      direction: 'horizontal',
      loop: true,
      autoplay: 1000,
      speed: 300,
      autoplayDisableOnInteraction: false,

      // 如果需要分页器
      pagination: {
        el: '.swiper-pagination',
      }
    });

  }

  confirm() {
    this.navCtrl.push(ConfirmOrderPage);
  }

}
