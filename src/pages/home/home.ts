import {Component, OnInit, ViewChild} from "@angular/core";
import {ModalController, NavController, Slides} from "ionic-angular";
import {LocationPage} from "../location/location";
import {DetailPage} from "../detail/detail";
import {CommonService} from "../service/CommonService";
import {config} from "../config/constants";
import {EventBusService} from "../service/EventBusService";
import {Combo, Product} from "../model";
// import Chart from "chartjs";

import Swiper from "swiper";

@Component({
  selector: 'page-home',
  templateUrl: './home.html'
})

export class HomePage implements OnInit {
  @ViewChild(Slides) slides: Slides;

  order: boolean = false;
  isClick: boolean = false;
  currentIndex: number = 1;
  isCategoryClick: boolean = false;
  currentCategoryIndex: number = 1;

  bannerList: Array<string[]> = [];
  comboList: Array<string[]> = [];
  productList: Array<string[]> = [];

  shoppingCartList: any = [];

  goToSlides() {
    this.slides.slideTo(1, 500);
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log(currentIndex);
  }

  ngOnInit(): void {
    console.log('home init');
    new Swiper('.swiper-container', {
      direction: 'horizontal',
      loop: true,
      autoplay: 5000,
      speed: 300,
      autoplayDisableOnInteraction: false,

      // 如果需要分页器
      pagination: {
        el: '.swiper-pagination',
      }
    });

    this.loadData();
  }

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, private service: CommonService, public eventBusService: EventBusService) {

  }


  ionViewDidEnter() {
    // let canvas = <HTMLCanvasElement> document.getElementById("myChart");
    // // 这里是关键, 不能写在constructor()中
    // var ctx = canvas.getContext('2d');
    // new Chart(ctx, {
    //   // The type of chart we want to create
    //   type: 'line',
    //
    //   // The data for our dataset
    //   data: {
    //     labels: ["January", "February", "March", "April", "May", "June", "July"],
    //     datasets: [{
    //       label: "My First dataset",
    //       backgroundColor: 'rgb(255, 99, 132)',
    //       borderColor: 'rgb(255, 99, 132)',
    //       data: [0, 10, 5, 2, 20, 30, 45],
    //     }]
    //   },
    //
    //   // Configuration options go here
    //   options: {}
    // });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.currentIndex = 1;
    this.isClick = true;
    this.currentCategoryIndex = 1;
    this.isCategoryClick = true;



  }


  getData() {

  }

  viewPlace() {
    let placeModal = this.modalCtrl.create(LocationPage);
    placeModal.present();
  }

  showTab(index: number) {
    this.currentIndex = index;
    this.isClick = true;
    console.dir(this.comboList);
  }

  showCategoryTab(index: number) {
    // 初始化当前种类索引为0
    this.currentCategoryIndex = index;
    this.isCategoryClick = true;

    // 加载单品数据
    this.service.getData('product/list?vemId=' + config.vemId + '&page=1&count=40&sort=1&categoryId=' + (index - 1)).subscribe(res => {
      this.productList = res.data;
      // console.log(this.productList);
    });
  }

  goDetail(goods: any) {
    this.navCtrl.push(DetailPage, {
      goods: goods
    });
  }

  loadData() {

    // 加载banner数据
    this.service.getData('sync/banners?vemId=' + config.vemId.toString()).subscribe(res => {
      this.bannerList = res;
      // console.log(this.bannerList)
    });

    // 加载combo数据
    this.service.getData('home/occasion?vemId=' + config.vemId.toString()).subscribe(res => {
      this.comboList = res.data;
      (this.comboList.length > 3) ? this.comboList.length = 3 : '';
    });

    // 加载单品种类数据
    this.service.getData('product/category?location=1').subscribe(res => {
      // console.log(res);
    });

    // 加载单品数据
    this.service.getData('product/list?vemId=' + config.vemId + '&page=1&count=40&sort=1&categoryId=0').subscribe(res => {
      this.productList = res.data;
      // console.log(this.productList)
    });
  }

  productSelectTimes: number = 1;
  comboSelectTimes: number = 1;
  // cartTotalPrice: number = 0;
  addTimes: number = 0;

  addToShoppingCart(merchandise: any) {
    alert('添加进购物车');
    let productId: Product;
    let comboId: Combo;

    if (merchandise.productId) {
      productId = merchandise.productId;
      let isExistFlag = this.shoppingCartList.find(product => product.productId === productId);
      if (isExistFlag) {
        merchandise.productSelectTimes += this.productSelectTimes;
      } else {
        merchandise.productSelectTimes = 1;
        this.shoppingCartList.push(merchandise);
      }
    } else if (merchandise.comboId) {
      comboId = merchandise.comboId;
      let index = this.shoppingCartList.findIndex(combo => combo.comboId === comboId);
      if (index > -1) {
        merchandise.comboSelectTimes += this.comboSelectTimes;
      } else {
        merchandise.comboSelectTimes = 1;
        this.shoppingCartList.push(merchandise);
      }
    }

    this.eventBusService.eventBus.next(this.shoppingCartList)
  }

}






























