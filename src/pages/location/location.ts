import {Component} from "@angular/core";
import {IonicPage, NavController, NavParams, ViewController} from "ionic-angular";
import {CommonService} from "../service/CommonService";

/**
 * Generated class for the LocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {

  // 当前位置的名称
  currentPlace: string = '';

  // 历史位置的名称集合
  historyPlace: Array<string[]> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public view: ViewController, private service: CommonService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationPage');
    this.service.getData('customer/hisLocation').subscribe((res) => {
      this.currentPlace = res.currentVem.vemName;
      this.historyPlace = res.historyVems;
    });
  }

  close() {
    this.view.dismiss();
  }

}
