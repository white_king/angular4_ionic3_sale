import {Component} from "@angular/core";
import {IonicPage, NavController, NavParams} from "ionic-angular";
import {FeedbackPage} from "../feedback/feedback";

/**
 * Generated class for the ConfirmOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-order',
  templateUrl: 'confirm-order.html',
})
export class ConfirmOrderPage {

  isSelected: boolean = false;
  isSelectedIndex: number = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmOrderPage');

    this.isSelected = false;
  }

  payAsyn() {
    this.navCtrl.push(FeedbackPage);
  }

  selectToPay(index: number) {
    this.isSelected = true;
    this.isSelectedIndex = index;
  }

}
