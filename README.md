# angular4_ionic3_salegoods
以angular4和ionic3作为主体框架搭建的一个电商平台webapp.

以angular4为主导框架，ionic3作为UI库，不过这里很少用它内置的UI，大多数还是自己写的。

其中以TypeScript作为开发语言，类比于JavaScript，语法上会更严格、更精准。

其中以webpack作为资源文件整理以及打包发布时对代码进行混淆，压缩操作。

其中用nginx做反向代理解决请求后台接口跨域问题。


#操作命令

cnpm install


#指定工程启动端口号9090
ionic serve --port 9090
