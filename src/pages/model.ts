export interface Message {
  errorMsg: string;
  status: number;
}

export interface Product {
  productId: number;
  name: string;
  subject: string;
  image: string;
  appPrice: number;
  vemPrice: number;
  inventory: number;
  leftInventory: number;
  inventoryHold: number;
  productSelectTimes?: number;
}

export interface Combo {
  comboId: number;
  appPrice: number;
  vemPrice: number;
  comboOrder:number;
  weekday: number;
  daytime: number;
  enableFlag: number;
  img: string;
  subject: string;
  name: string;
  products?: string;
  comboSelectTimes?: number;
}

export interface Config {
  serviceUrl: string;
  vemId: number;
  token: string;
  source: number;
}



