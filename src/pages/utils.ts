export const Utils = {

  // 正则验证手机号码
  testPhone(phone) {
    let reg = /^1[3|4|5|7|8][0-9]{9}$/;

    return reg.test(phone)
  }
};
